// Get the four DOM quadrants
const quadrants = document.querySelectorAll('.quadrant'),
      startBtn = document.getElementById('start-button');
let flashMessage = document.getElementById('flash-message');

// Vars
let CPUSequence,
    userSequence,
    level = 1,
    checkForClick,
    userCanEnter = false;

const quadrantsMap = {
    'top-left': 0,
    'top-right': 1,
    'bottom-left': 2,
    'bottom-right': 3
}

// Add the listeners
quadrants.forEach(() => addEventListener('click', highlightQuadrant));
startBtn.addEventListener('change', startGame);

/**
 * Return a random number between min and max
 * 
 * @param {int} min 
 * @param {int} max
 * 
 * @returns {int} The random number 
 */
function randomRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Hightlight a quadrant either by being clicked or programatically
 * via index on return value of querySelectorAll('.quadrant');
 * 
 * @param {obj} e A DOM element
 */
function highlightQuadrant(e) {
    // Quadrant has been clicked
    if (e.target) {
        if (userCanEnter) {
            e.target.classList.add('active');
            userSequence.push(quadrantsMap[e.target.id]);
            
            setTimeout(function() {
                e.target.classList.remove('active');
                checkForWin();
            }, 200);
        }
    }
    else {
        e.classList.add('active');

        setTimeout(function() {
            e.classList.remove('active');
        }, 600);
    }
}

/**
 * Check if a user has won
 */
function checkForWin() {   
    clearTimeout(checkForClick);

    // After 2 seconds of inactivity after last quadrant selection
    checkForClick = setTimeout(function() {
        // return _.isEqual(CPUSequence, userSequence)
        if (JSON.stringify(CPUSequence) === JSON.stringify(userSequence)) {
            level += 1;
            startGame();
        }
        else {
            flashMessage.textContent = 'Simon says :(';
            setTimeout(function() {
                startBtn.checked = false;
                startGame();
            }, 2000);
        }
    }, 2000);
}

/**
 * Create a dynamic sequence
 */
function createSequence() {
    for (let i = 0; i < level; i++) {
        CPUSequence.push(randomRange(0, 3));
    }
}

/**
 * Remove all highlighted quadrants
 */
function removeAllHighlights() {
    quadrants.forEach((el) => el.classList.remove('active'));
}

/**
 * Display the CPU sequence based on the created sequence
 */
function displayCPUSequence() {
    userCanEnter = false;
    removeAllHighlights();

    CPUSequence.forEach(function(value, index) {
        let id = setTimeout(function() {
            highlightQuadrant(quadrants[value]);

            if ((index + 1) === (CPUSequence.length)) {
                userCanEnter = true;
            }

        }, 1000 * index);
    });
}

/**
 * Start game
 */
function startGame() {
    CPUSequence = [];
    userSequence = [];

    if (startBtn.checked) {
        createSequence();
        displayCPUSequence();
        flashMessage.textContent = 'Level ' + level;
    }
    else {
        level = 1;
        flashMessage.textContent = 'Ready?';
    }
}