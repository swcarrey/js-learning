import {createList} from './createList.js';

// createList() function now available here
const names = ['Rick', 'Andy', 'Sarah'];
createList(names);
