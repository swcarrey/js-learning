'use strict';

const createList = function(names) {

    const container = document.getElementById('container');
    const ul = document.createElement('ul');
    
    container.appendChild(ul);
    
    names.forEach(function(item) {
        let listItem = document.createElement('li');
        listItem.textContent = item;
        ul.appendChild(listItem);
    });    
    
};

export {createList};