/**
 * Make a recipe for hummus
 * 
 * @param {int} factor 
 * @returns void
 */
const hummus = function(factor) {

    /**
     * 
     * @param {int} amount Amount to use
     * @param {string} unit Unit to measure 
     * @param {string} name Description of ingredient
     * @returns void
     */
    const ingredient = function(amount, unit, name) {
        let ingredientAmount = amount * factor;
      
        if (ingredientAmount > 1) {
            unit += "s";
        }
        console.log(`${ingredientAmount} ${unit} ${name}`);
    };

    ingredient(1, "can", "chickpeas");
    ingredient(0.25, "cup", "tahini");
    ingredient(0.25, "cup", "lemon juice");
    ingredient(1, "clove", "garlic");
    ingredient(2, "tablespoon", "olive oil");
    ingredient(0.5, "teaspoon", "cumin");
};


hummus(1);
/*

1 can chickpeas
0.25 cup tahini
0.25 cup lemon juice
1 clove garlic
2 tablespoons olive oil
0.5 teaspoon cumin

*/