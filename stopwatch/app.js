const   display = document.getElementById('display'),
        startBtn = document.getElementById('start'),
        stopBtn = document.getElementById('stop'),
        resetBtn = document.getElementById('reset');

        startBtn.addEventListener('click', start);
        stopBtn.addEventListener('click', stop);
        resetBtn.addEventListener('click', reset);

let time = 0,
    offset,
    interval,
    running = false;

/**
 * Start the stopwatch
 */
function start() {
    interval = setInterval(update, 10);
    offset = Date.now();
    running = true;
}

/**
 * Stop the stopwatch
 */
function stop() {
    clearInterval(interval);
    interval = null;
    running = false;
}

/**
 * Reset the stopwatch
 */
function reset() {
    time = 0;
    update();
}

/**
 * Runs every 10ms. If stopwatch is running, sets time to the
 * amount of time passed since pressing start and renders the
 * time inside div#display 
 */
function update() {
    if (running) {
        time += delta();
    }     
    display.textContent = render(time);
}

/**
 * calculates how much time has passed since
 * pressing start
 */
function delta() {
    let now = Date.now();
    let timePassed = now - offset;
    offset = now;
    
    return timePassed;
}

/**
 * Takes a timestamp and returns string formatted as HH:MM:SS:MS
 * @param {int} time The timestamp
 * 
 * @return string
 */
function render(time) {
    time = new Date(time);
    
    hours = padZero(time.getUTCHours());
    minutes = padZero(time.getUTCMinutes());
    seconds = padZero(time.getUTCSeconds());
    milliseconds = padZero(time.getUTCMilliseconds() / 10 | 0);

    return hours + ':' + minutes + ':' + seconds + ':' + milliseconds;
}

/**
 * Takes a number and adds leading zero if under 10
 * @param {int} time The timestamp 
 * 
 * @return string
 */
function padZero(time) {
    return (time < 10) ? '0' + time : '' + time;
}
