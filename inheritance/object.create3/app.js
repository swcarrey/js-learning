const human = {
    create(name, age) {
        const instance = Object.create(this);
        instance.name = name;
        instance.age = age;
        return instance;
    }
};

const sam = human.create('Sam', 39);
console.log(sam.age);
