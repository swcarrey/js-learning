const food = {
  init: function(name, calories, type, freeDonuts) {
    this.name = name;
    this.calories = calories;
    this.type = type;
    this.freeDonuts = freeDonuts;
  }
};

const waffles = Object.create(food);
const carrots = Object.create(food);

waffles.init('waffles', 1000, 'breakfast snack', false);
carrots.init('carrots', 20, 'healthy snack', true);

/**
 * Add method to prototype of objects created with food object
 * @param {bool} bool 
 */
food.giveFreeDonuts = function(bool) {
  this.freeDonuts = bool;
};

console.log(waffles);

waffles.giveFreeDonuts(true);

console.log(carrots);

console.log('-------GAVE FREE DONUTS TO WAFFLES-------');

console.log(waffles);
