const Person = {
  name: 'Sam',
  age: 38,
  getName: function() {
    console.log(this.name);
  }
}

const Athlete = Object.create(Person, {
  event: {
    value: 'High jump'
  },
  getEvent: {
    value: function() {
      console.log(this.event);
    }
  }
});

const sam = Object.create(Athlete);
sam.getName();
