// 'Sam', 'Carrey', 'year/month/date'
const Person = function(firstname, lastname, dob) {
  return new Person.init(firstname, lastname, dob);
};

Person.init = function(firstname, lastname, dob) {
  this.firstname = firstname;
  this.lastname = lastname;
  this.dob = dob;
}

/**
 * So let's make the Person.prototype
 */
Person.prototype = {
  // Prob completely not accurate enough here...
  getAge: function() {
    const now = new Date();
    const dob = new Date(this.dob);
    // The age in years
    let age = now.getFullYear() - dob.getFullYear();
    if (now.getMonth() < dob.getMonth()) {
      age--;
    }
    return age;
  },

  getFullName: function() {
    return this.firstname + ' ' + this.lastname;
  } 
};

/**
 * Pointing the Person.init.prototype to the same
 * spot in memory as Person.prototype
 * */
Person.init.prototype = Person.prototype;

/**
 * ***************************
 */
const sam = Person('Sam', 'Carrey', '1979-11-20');
console.log(sam.__proto__=== Person.init.prototype); // true
console.log(sam.__proto__=== Person.prototype); // true

console.log(sam.getFullName()); // Sam Carrey
console.log(sam.getAge()); // 38

