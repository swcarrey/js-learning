/**
 * Game state
 */
let gameOn = false;

/**
 * Overlay for win
 */
const overlay = document.getElementById('overlay');
overlay.classList.add('hide');

/**
 * Get cards wrapper
 */
const cardsParent = document.getElementById('cards');

/**
 * Get HTML collection of cards
 */
const cards = document.querySelectorAll('.card');

/**
 * Start button
 */
const startBtns = document.querySelectorAll('.start');

/**
 * Get winning card
 */
const winCard = document.getElementById('win_card');

/**
 * Add event listeners
 */
cards.forEach((el) => el.addEventListener('click', selectCard));
startBtns.forEach((el) => el.addEventListener('click', startGame));

/**
 * Functions 
 */
function selectCard(e) {
  if (gameOn) {
    flipCard(e);
    checkForWin(e);
  }
}

function flipCard(e) {
  // Remove flip class from all cards
  cards.forEach((el) => el.classList.remove('flip'));

  // Add flip class to current selection
  e.target.parentNode.classList.toggle('flip');
}

function startGame(e) {
  // Game started
  gameOn = true;

  // Hide overlay if playing again
  if (e.target.id === 'play_again') {
    overlay.classList.add('hide');
  }

  // Show Dennis
  winCard.classList.add('flip');

  setTimeout(() => {
    winCard.classList.remove('flip');

    setTimeout(switchCards, 1000);
  
  }, 1000);
}

function switchCards() {
  /**
   * Animate them
   */
  cardsParent.classList.add('animate');

  setTimeout(() => cardsParent.classList.remove('animate'), 5000);

  /**
   * Reposition winning card
   */
  cardsParent.insertBefore(winCard, cards[randomRange(0, cards.length)]);
}

function randomRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function checkForWin(e) {
  if (e.target.parentNode.id === 'win_card') {
    setTimeout(() => overlay.classList.remove('hide'), 1500);
  }
}