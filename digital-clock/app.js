/**
 * Get DOM elements
 */
const display = document.getElementById('display'),
      secondsContainer = document.getElementById('secondsContainer'),
      spinner = document.getElementById('spinner');

/**
 * Start the clock
 */
function start() {
    // Update the clock initially with current time
    update();
    // Update every 1 sec
    interval = setInterval(update, 1000);
    // Start the animations
    spinner.classList.add('active');
    secondsContainer.classList.add('active');
};

/**
 * Updates the clock display
 */
function update() {
    // Update time now
    time = Date.now();
    // Get the current time object containing hrs/mins/secs
    let clockVars = render(time);
    // Update the display
    display.textContent = clockVars.hours + ':' + clockVars.minutes;
    secondsContainer.textContent = clockVars.seconds;
}

/**
 * Takes a timestamp and returns string formatted as HH:MM:SS:MS
 * @param {int} time The timestamp
 * 
 * @return object The individual values for hrs/mins/secs
 */
function render(time) {
    time = new Date(time);
    
    hours = padZero(time.getUTCHours());
    minutes = padZero(time.getUTCMinutes());
    seconds = padZero(time.getUTCSeconds());

    return {
        hours: hours,
        minutes: minutes,
        seconds: seconds
    }
}

/**
 * Takes a number and adds leading zero if under 10
 * @param {int} time The timestamp 
 * 
 * @return string
 */
function padZero(time) {
    return (time < 10) ? '0' + time : '' + time;
}

// Let's go!
start();
