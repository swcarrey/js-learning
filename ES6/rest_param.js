// Rest parameter
function loadsOfArgs(...args) {
    return args;
}

function isAnArray(arr) {
    return Object.prototype.toString.call(arr) === '[object Array]';
}

const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
console.log(isAnArray(arr));

// Spread operator
const result = loadsOfArgs(...arr);

console.log(result); // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] 

