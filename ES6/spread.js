const nums = [1, 10, 30, 1000, 34, 23];

// Find the max, old skool
function getMaxNum(arr) {
    return arr.reduce(function(acc, cur) {
        return Math.max(acc, cur);
    });
}
console.log(getMaxNum(nums)); // 1000

// OR more ES6 style...
function getMaxNumES6(arr) {
    return arr.reduce((acc, cur) => Math.max(acc, cur)); // Nice :)
}
console.log(getMaxNumES6(nums)); // 1000

/**
 * ES6 spread operator makes it even easier
 */
function getMaxNumsViaSpread(nums) {
    return Math.max(...nums); // Holy crap!
}
console.log(getMaxNumsViaSpread(nums)); // 1000

/**
 * What else can we use this for?
 */
const names = ['Bowser', 'Mario', 'Toad', 'Yoshi'];
const [player_1, player_2, player_3, player_4] = names;
console.log(player_1); // Bowser
console.log(player_2); // Mario
console.log(player_3); // Toad
console.log(player_4); // Yoshi

