/**
 * Take the string object
 * Create variable called length
 * Assign value of object.length to length variable
 * 
 * Weird...
 */
const { length } = "Sam C";
console.log(length); // 5


const person = {
    name: 'Sam',
    country: 'UK',
    twitter: '@swcarrey'
};
/**
 * Grab the object properties easy
 */
const {name, country, twitter} = person;
console.log(name, country, twitter); // Sam UK @swcarrey

