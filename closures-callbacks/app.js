// ES6
const sayHi = (name, ms) =>
  () => {
    setTimeout(() => console.log('Hi', name), ms);
    (function sayHiFirst(){
      console.log('I said hi first even though I was after the top code block!');
    }());
  }

const sayHiToSam = sayHi('Sam', 1000);
sayHiToSam();
