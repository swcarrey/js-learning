/**
 * Thanks to:
 * https://stackoverflow.com/questions/15521081/jquery-animate-in-pure-javascript
 * https://stackoverflow.com/questions/15876754/infinity-loop-slider-concepts
 */

 window.onload = function() {
  slideshow();
 };

 const slideshow = function() {
  // DOM
  const prev = document.getElementById('prev'),
        next = document.getElementById('next'),
        slideWindow = document.getElementById('slide-window'),
        slides = document.querySelectorAll('.slide'),
        numberOfSlides = slides.length,
        /**
         * Clone last image before first
         * CLone first image after last
         */
        last = slides[numberOfSlides - 1].cloneNode(true),
        first = slides[0].cloneNode(true);


  // Add clones to DOM
  slideWindow.insertBefore(last, slideWindow.firstElementChild);
  slideWindow.appendChild(first);


  // Event listeners
  prev.addEventListener('click', function() {
    switchSlide(-1);  
  });

  next.addEventListener('click', function() {
    switchSlide(1);  
  });


  // Base image size
  const imageWidth = 600;


  // Current image
  let currentSlide = 1;


  // Currently animating
  let isAnimating = false;


  /**
   * Move slide forwards (1) or backwards (-1)
   * @param {int} n 1 or -1 (forwards or backwards)
   */
  function switchSlide(n) {
    if (!isAnimating) {

      const currentPosition = -imageWidth * currentSlide;
      animate(currentPosition, n, resetPosition);
    }
  }


  /**
   * When switched from first image to last-cloned (backward) or
   * from last image to first-cloned (forward)
   */
  function resetPosition() {
    currentSlide = (currentSlide === 0) ? slides.length : 1; 
    slideWindow.style.transform = `translateX(${-imageWidth * currentSlide}px)`;
  }


  /**
   * Animate the slides
   * 
   * @param {int} currentPosition Position in px of current slide 
   * @param {int} n 1 or -1 (forwards or backwards)
   * @param {*} callback Conditional function to run after animation finishes
   */
  function animate(currentPosition, n, callback) {
    isAnimating = true;

    var start = currentPosition;
    var stepper = 0;
    function frame() {
        // Update stepper
        stepper += 5;
        slideWindow.style.transform = `translateX(${currentPosition + (-stepper * n)}px)`;

        // Finish condition
        if (stepper == 600) {
            clearInterval(sliding);
            isAnimating = false;
        
          /** 
           * Check whether the current slide has exceeded the maximum or minimum
           * length of slides i.e. on a clone image then run  callback to immediately
           * move slides wrapper back to 'real' image
           */          
          cycle = !!(currentSlide === 0 || currentSlide > slides.length);

          if (cycle) {
            callback();
          }
        }
    }
    var sliding = setInterval(frame, 0.1);

    // Update current slide
    currentSlide += n;
  }
}