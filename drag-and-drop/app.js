const fill = document.querySelector('.fill');
const empties = document.querySelectorAll('.empty');

// Fill listeners
fill.addEventListener('dragstart', dragStart);
fill.addEventListener('dragend', dragEnd);

// Loop through empties and call drag events
for (const empty of empties) {
  empty.addEventListener('dragover', dragOver);
  empty.addEventListener('dragenter', dragEnter);
  empty.addEventListener('dragleave', dragLeave);
  empty.addEventListener('drop', dragDrop);
}

// Drag functions
function dragStart() {
  // Add css effect on image div
  this.className += ' hold' ;
  /**
   * Hide image inside fill div after drag starts
   * !!! SUPER IMPORTANT !!!
   */
  setTimeout(() => (this.className = 'invisible'), 0);
}

function dragEnd() {
  this.className = 'fill';
}

function dragOver(e) {
  e.preventDefault();
}

function dragEnter(e) {
  e.preventDefault();
  this.className += ' hovered';  
}

function dragLeave(e) {
  this.className = 'empty';  
}

function dragDrop() {
  this.className = 'empty';
  this.append(fill);
}
