/**
 * Actions
 * @param {object} state 
 */
const run = (state) => ({
  run: () => console.log('You ran ' + state.distance + ' metres')
});

const jump = (state) => ({
  jump: () => console.log('You jumped ' + state.height + ' metres in the air!')
});

/**
 * Assign actions to Person object
 * @param {string} name 
 */
const Person = (name) => {
  let state = {
    name: name,
    height: 120,
    distance: 100
  }

  return Object.assign(
    {},
    run(state),
    jump(state)
  )
};

/**
 * Create Person object
 */
const sam = Person('Sam');

/**
 * Make Person do stuff!
 */
sam.jump();
sam.run();
