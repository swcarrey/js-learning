const arr = [
    {name: 'Sam', age: 39},
    {name: 'Bill', age: 45},
    {name: 'Anne', age: 28},
    {name: 'Anne', age: 28}
];

function sortByName(a, b) {
    let nameA = a.name.toUpperCase();
    let nameB = b.name.toUpperCase();
    
    if (nameA < nameB) {
        return -1;
    }

    if (nameA > nameB) {
        return 1;
    }
    
    return 0;
}

// arr.sort(sortByName);
// console.log(arr);

/**
 * OUTPUTS
 * [ { name: 'Anne', age: 28 },
  { name: 'Anne', age: 28 },
  { name: 'Bill', age: 45 },
  { name: 'Sam', age: 39 } ]
 */


// So we can compare string with '<' or '>'
console.log(arr[0].name.toUpperCase() /* SAM */ < arr[1].name.toUpperCase() /* BILL */); // false
console.log(arr[0].name.toUpperCase() /* SAM */ > arr[1].name.toUpperCase() /* BILL */); // true

// SWEET :)
