var person = {
  firstname: 'John',
  lastname: 'Doe',
  getName: function() {
      return this.firstname;
  }
};

/**
 * Check if a method exists on an object (itself or inherited)
 * 
 * @param {object} obj The object 
 * @param {string} fn The function name as string
 * @param {bool} hasInheritedProp (optional) true to include inherited properties
 * 
 * @return true if exists, false if non-existant
 */
function doesFunctionExist(obj, fn, hasInheritedProp = false) {
  // Check whether object has own property by default
  if (!hasInheritedProp) {
    hasInheritedProp = obj.hasOwnProperty(fn);
  }
  // Short circuit evaluation
  return (hasInheritedProp && typeof obj[fn] === 'function');
}

// Test cases
const result = doesFunctionExist(person, 'toString');
const result2 = doesFunctionExist(person, 'toString', true);
const result3 = doesFunctionExist(person, 'getName');
const result4 = doesFunctionExist(person, 'getName', true);
const result5 = doesFunctionExist(person, 'iDontExist');
const result6 = doesFunctionExist(person, 'iDontExist', true);

// Test results
console.log(result); // FALSE
console.log(result2); // TRUE
console.log(result3); // TRUE
console.log(result4); // TRUE
console.log(result5); // FALSE
console.log(result6); // FALSE