/**
 * Sams excellent Bill & Ted program
 */
const arr = [
  {name: 'Bill', age: 17, town: 'San Dimas', saying: 'Outstanding Rufus'},
  {name: 'Ted', age: 18, town: 'San Dimas', saying: 'Excellent'},
  {name: 'Rufus', age: 50, town: 'Utopian future', saying: 'They do get better'}
];

/**
 * Capitalize a single word
 * 
 * @param {string} str The word to capitalize
 * 
 * @return The capitalized word
 */
const capitalizeWord = (str) => str.charAt(0).toUpperCase() + str.slice(1);

/**
 * Find the value of a property in an array of objects
 * 
 * @param {string} personName 
 * @param {string} propertyVal
 * 
 * @return {string} 
 */
const getValue = function(personName, propertyVal) {
    // Return matched array item
    const result = arr.filter(item => item.name === personName)
                    // Return the value of the property
                    .map(item => item[propertyVal])
                    // Reduce from array to a string
                    .reduce((prev, value) => (prev || 0 ) + value);

  propertyVal = capitalizeWord(propertyVal);
  return `${propertyVal} for ${personName} is ${result}`; 
} 

console.log(getValue('Bill', 'town')); // Town for Bill is San Dimas