// const nums = [100, 50, 25, 250];

// const average = nums.reduce((acc, cur, idx, arr) => (acc + cur) / arr.length);

// console.log(average);

// Let's take an array
const arr = [
    'Bill', 'Ben', 'Ted', 'Jen'
];

/*
...and convert to desired output

{
    person1: {
        name: 'Bill'
    },
    person2: {
        name: 'Ben'
    },
    person3: {
        name: 'Ted'
    },
    person4: {
        name: 'Jen'
    }
};

*/

const reduced = arr.reduce(function(acc, cur, idx, arr) {
    // Set up the string to use for property name
    let propertyName = 'person' + (idx + 1);
    /**
     * Set the new property name to object wih
     * key=name, value=current array item
     */
    acc[propertyName] = { name: cur};
    // Return acc back into reduce function
    return acc;
}, {});

console.log(reduced);
