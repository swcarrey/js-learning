const   display = document.getElementById('display'),
        startBtn = document.getElementById('start'),
        stopBtn = document.getElementById('stop'),
        resetBtn = document.getElementById('reset');

        startBtn.addEventListener('click', start);
        stopBtn.addEventListener('click', stop);
        resetBtn.addEventListener('click', reset);

let totalSeconds = (function(){
    let mins = prompt('How many minutes to countdown?');
    let seconds = mins * 60;
    updateDisplay(seconds);
    return seconds;
})();

let interval;
    resetSeconds = totalSeconds;

/**
 * Start the countdown
 */
function start() {
    // immediately update
    updateDisplay(totalSeconds);
    // then every second
    interval = setInterval(function() {
        totalSeconds--;
        updateDisplay(totalSeconds);
    }, 1000);
}

/**
 * Stop the countdown
 */
function stop() {
    clearInterval(interval);
}

/**
 * Reset the countdown
 */
function reset() {
    totalSeconds = resetSeconds;
    updateDisplay(resetSeconds);
}

/**
 * Update the display
 */
function updateDisplay(totalSeconds) {
    let minutes = padZero(Math.floor(totalSeconds / 60));
    let seconds = padZero(totalSeconds % 60);
    
    display.textContent = minutes + ':' + seconds;
    
    if (totalSeconds === 0) {
        stop();
    }
}

/**
 * Takes a number and adds leading zero if under 10
 * @param {int} time The timestamp 
 * 
 * @return string
 */
function padZero(time) {
    return (time < 10) ? '0' + time : '' + time;
}
