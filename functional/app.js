// function mapEachItem(arr, fn) {
//   const result = [];
//   const length = arr.length;
  
//   for (let i = 0; i < length; i++) {
//     result.push(
//       fn(arr[i])
//     );
//   }

//   return result;
// }

const doubleArray = (item) => item * 2;

// function tripleArray(item) {
//   return item * 3;
// }

const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// const doubleArr = mapEachItem(arr, doubleArray);
// const tripleArr = mapEachItem(arr, tripleArray);

// console.log(doubleArr);
// console.log(tripleArr);

// Quicker to use map...
const arrDoubled = arr.map(doubleArray);

console.log(arrDoubled);
