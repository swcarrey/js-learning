const names = ['tom', 'dick', 'harry'];

const modifyNames = (arr, fn) => {
    const results = [];
    arr.forEach(element => results.push(
        fn(element))
    );
    return results;
};

const capitalize = (name) => name.toUpperCase();
const reverse = (name) => name.split('').reverse().join('');

capitalNames = modifyNames(names, capitalize);
reverseNames = modifyNames(names, reverse);

console.log(names);
console.log(capitalNames);
console.log(reverseNames);
