(function() {

  /**
   * Regex values
   */
  const regex = {
    name: /^[A-Z][a-z]+\s[A-Z][a-z]+$/,
    phone: /^[0-9]{11}$/,
    email: /^[A-Z0-9a-z\.]+\@[A-Z0-9a-z]+\.[A-Za-z\.]+$/
  };

  // Cache the DOM
  const form = document.getElementById('regexForm');

  // Add listeners
  form.addEventListener('keyup', checkField);

  // Initial values
  let valid = false;
  let initial_email = '';

  /*******************************
   * Functions
   *******************************/

  // Identify which field a user is on
  function checkField(e) {
    const input = e.target,
          inputName = input.name;

    if (inputName === 'email') {
      initial_email = input.value;

      /**
       * Changing initial email after confirming it
       */
      if (form.confirm_email.value !== '') {
        generateFeedback(form.confirm_email, form.confirm_email.value === initial_email);
      }
    }

    if (input.name === 'confirm_email') {
      valid = input.value === initial_email;
    }
    else {    
      valid = regex[inputName].test(input.value);
    }

    generateFeedback(input, valid);
  }

  // Generate feedback
  function generateFeedback(input, isValid) {
    if (isValid) {
      input.classList.add('valid');
      input.classList.remove('error');
      input.nextElementSibling.classList.add('valid');
    }
    else {
      input.classList.add('error');
      input.classList.remove('valid');
      input.nextElementSibling.classList.remove('valid');
    }
  }

}());
