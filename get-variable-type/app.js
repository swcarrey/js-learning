const a = 3;
console.log(typeof a); // number

const b = 'Hello';
console.log(typeof b); // string

const c = {};
console.log(typeof c); // object

const d = [];
console.log(typeof d); // object...
console.log(Object.prototype.toString.call(d)); // [object Array]

function Person(name) {
  this.name = name;
}

const e = new Person('Sam');
console.log(typeof e); // object
console.log(e instanceof Person); // true

console.log(typeof undefined); // undefined
console.log(typeof null); // object??! Whaaaa. BUG.

const f = function() {};
console.log(typeof f); // function