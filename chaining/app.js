const Person = {
  init: function(name, age) {
    this.name = name;
    this.age = age;
    return this;
  },
  greet: function() {
    return 'Hi ' + this.name + '. You are: ' + this.age;
  }
};

const sam = Object.create(Person);
console.log(sam.init('Sam', 38).greet());