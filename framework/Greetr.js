(function(global, $) {
  
  /**
   * The Greetr object
   * 
   * @param {string} firstname
   * @param {string} lastname
   * @param {string} language en, es, de etc.
   */
  const Greetr = (firstname, lastname, language) => new Greetr.init(firstname, lastname, language);

  /**
   * Supported languages
   */
  const supportedLangs = ['en', 'es', 'de'];

  /**
   * Messages
   */
  const greetings = {
    en: 'Hi',
    es: 'Hola',
    de: 'Hallo'
  };

  const formalGreetings = {
    en: 'Good day to you',
    es: 'Buen día para usted señor',
    de: 'Guten Tag für Sie, Herr'
  };

  const logMessages = {
    en: 'Logged in',
    es: 'Inicio de sesión',
    de: 'Eingeloggt'
  };

  /** 
   * Framework methods
   */
  Greetr.prototype = {

    fullName: function() {
      return this.firstname + ' ' + this.lastname;
    },

    validateLanguage: function() {
      // Not found in supportedLangs array
      if (supportedLangs.indexOf(this.language) === -1) {
        throw 'Invalid language';
      }
    },

    greeting: function() {
      return greetings[this.language] + ' ' + this.firstname;
    },

    formalGreeting: function() {
      return formalGreetings[this.language] + ' ' + this.fullName();
    }

  };

  /**
   * Function constructor
   */
  Greetr.init = function(firstname, lastname, language) {
    this.firstname = firstname || '';
    this.lastname = lastname || '';
    this.language = language || supportLanguages[0];
  };

  /**
   * Set the prototype for objects created with the Greetr.init function 
   * constructor to Greetr.prototype
   */
  Greetr.init.prototype = Greetr.prototype;

  /**
   * For access on the global object using Greetr() or alias G$()
   */
  global.Greetr = global.G$ = Greetr;

})(window, jQuery);
