function diffArray(arr1, arr2) {
    /**
     * Concatenate both arrays
     * Filter them
     * Return items that are NOT in both
     */
    return [...arr1, ...arr2].filter(el => !(arr1.indexOf(el) >= 0 && arr2.indexOf(el) >= 0));
}

const result = diffArray([1, 2, 3, 5], [1, 2, 3, 4, 5]);
//const result = diffArray(["diorite", "andesite", "grass", "dirt", "pink wool", "dead shrub"], ["diorite", "andesite", "grass", "dirt", "dead shrub"]);

console.log(result);
