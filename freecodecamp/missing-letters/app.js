function fearNotLetter(str) {
    const letters = 'abcdefghijklmnopqrstuvwxyz';
    const firstChar = letters.indexOf(str.charAt(0));
    const part = letters.slice(firstChar, firstChar + str.length);

    // No missing letters
    if (part === str) {
        return undefined;
    }

    // Find missing letter
    return part.split('').filter(el => !(str.indexOf(el) >= 0)).join();
}

fearNotLetter("abce");