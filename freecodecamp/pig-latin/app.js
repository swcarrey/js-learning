// https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/intermediate-algorithm-scripting/pig-latin

function translatePigLatin(str) {
    // Get first vowel: -1 if no vowels else index of first vowel
    const firstVowel = str.search(/[aeiou]/);
    // Has no vowels
    if (firstVowel === -1) {
        return str + 'ay';
    }
    // Begins with vowel
    else if (firstVowel === 0) {
        return str + 'way';
    }
    else {
        return str.slice(firstVowel) + str.slice(0, firstVowel) + 'ay';
    }
}
  
//const result = translatePigLatin("consonant");
// const result = translatePigLatin("eight");
//const result = translatePigLatin("glove");
const result = translatePigLatin("algorithm");

console.log(result);

