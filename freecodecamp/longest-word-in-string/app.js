function findLongestWordLength(str) {
    // String to array, sorted longest word first, return length of first word
    return str.split(' ').sort((a, b) => b.length - a.length).shift().length;
}
  
const result = findLongestWordLength("The quick brown fox jumped over the lazy dog");
console.log(result);
