/*
Create a function that sums two arguments together. If only one argument is provided, then return a function that expects one argument and returns the sum.

For example, addTogether(2, 3) should return 5, and addTogether(2) should return a function.

Calling this returned function with a single argument will then return the sum:

var sumTwoAnd = addTogether(2);

sumTwoAnd(3) returns 5.

If either argument isn't a valid number, return undefined.
*/

function addTogether() {

    function isANumber(testVal) {
        return typeof testVal === 'number';
    }

    let args = Array.from(arguments);

    // Bail out early if any argument is not a number
    if (!args.every(isANumber)) {
        return undefined;
    }

    // Now we know we got numbers, 2 of them?
    if (args.length > 1) {
        return args[0] + args[1];
    }
    // Only one, check again for valid number within returned function
    else {
        return function(x) {
            if (isANumber(x)) {
                return x + args[0];
            }
        }
    }
}
  
// const result = addTogether(2, 3); // should return 5.
// const result = addTogether(2)(3); // should return 5.
// const result = addTogether("http://bit.ly/IqT6zt"); // should return undefined.
// const result = addTogether(2, "3"); // should return undefined.
const result = addTogether(2)([3]); // should return undefined.

console.log(result); // undefined

