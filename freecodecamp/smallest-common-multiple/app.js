/*
Find the smallest common multiple of the provided parameters that can be evenly divided by both, as well as by all sequential numbers in the range between these parameters.

The range will be an array of two numbers that will not necessarily be in numerical order.

For example, if given 1 and 3, find the smallest common multiple of both 1 and 3 that is also evenly divisible by all numbers between 1 and 3. The answer here would be 6.
*/
function smallestCommons(arr) {
    // Create new array, don't modify original!
    let newArr = [...arr];
    // Sort the array biggest -> smallest
    newArr.sort((a, b) => b - a);
    // Destructure the array and grab biggest and smallest values
    let [biggest, smallest] = newArr;
    // Empty array and recreate including all values between range values
    newArr = [];
    for (let i = biggest; i >= smallest; i--) {
        newArr.push(i);
    }

    let i = biggest;
    // Loop until we find LCM
    while(true) {
        // Is every element is evenly divisible into i?
        if (newArr.every(el => i % el === 0)) {
            // We found it! Exit loop, return the LCM
            return i;
        }
        i++;
    }
}


//const result = smallestCommons([1, 5]) // should return a number.
// const result = smallestCommons([1, 5]) // should return 60.
// const result = smallestCommons([5, 1]) // should return 60.
// const result = smallestCommons([2, 8]) // should return 840.
const result = smallestCommons([1, 13]) // should return 360360.
// const result = smallestCommons([23, 18]) // should return 6056820.

console.log(result); // 360360

// FCC advanced - NOT MY CODE - more efficient
function smallestCommons(arr) {

    // range
    let min = Math.min.apply(null, arr);
    let max = Math.max.apply(null, arr);
  
    let smallestCommon = lcm(min, min + 1);
  
    while(min < max) {
      min++;
      smallestCommon = lcm(smallestCommon, min);
    }
  
    return smallestCommon;
  }
  
  /**
   * Calculates Greatest Common Divisor
   * of two nubers using Euclidean algorithm
   * https://en.wikipedia.org/wiki/Euclidean_algorithm
   */
  function gcd(a, b) {
    while (b > 0) {
      let tmp = a;
      a = b;
      b = tmp % b;
    }
    return a;
  }
  
  /**
   * Calculates Least Common Multiple
   * for two numbers utilising GCD
   */
  function lcm(a, b) {
    return (a * b / gcd(a, b));
  }
  