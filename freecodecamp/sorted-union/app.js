/**
 * @param arr One or more arrays
 */
function uniteUnique(arr) {
    let newArr = [];

    // Flatten arg arrays
    for (let i = 0; i < arguments.length; i++) {
        newArr = newArr.concat(arguments[i]);
    }

    // Filter unique elements
    return newArr.filter(function(el, idx) {
        return newArr.indexOf(el) === idx;
    });
}

const result = uniteUnique([1, 3, 2], [5, 2, 1, 4], [2, 1]);
console.log(result); // [1, 3, 2, 5, 4]


// OR ES6 style


/**
 * @param arr One or more arrays
 */
function uniteUnique2(...arr) {
    // Flatten arg arrays using spread
    let newArr = [].concat(...arr);
    // Create a Set (values are unique) from flattened array, unpack and return new array
    return [...new Set(newArr)];
}

const result2 = uniteUnique2([1, 3, 2], [5, 2, 1, 4], [2, 1]);
console.log(result2); // [1, 3, 2, 5, 4]

