// function randomWholeNum(max) {

//   // Only change code below this line.

//   return Math.floor(Math.random() * max) + 1;
// }

// // Random number between 0-10
// console.log(randomWholeNum(10));


/////////////////////////////////////////////
// Example

/**
 *  https://forum.freecodecamp.org/t/generate-random-whole-numbers-within-a-range-2/149400/3
 */
function ourRandomRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Random number between 0-2 (inclusive of both)
console.log(ourRandomRange(2, 5));
