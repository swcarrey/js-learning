/*
Given the array arr, iterate through and remove each element starting from the first element (the 0 index) until the function func returns true when the iterated element is passed through it.

Then return the rest of the array once the condition is satisfied, otherwise, arr should be returned as an empty array.
*/
function dropElements(arr, func) {
    // Don't modify original array
    let newArr = [...arr];
    let arrLength = newArr.length;

    for (let i = 0; i < arrLength; i++) {
        // Exit loop when true
        if (func(newArr[0])) {
            break;
        }
        else {
            newArr.shift();
        }
    }
    return newArr;
}

// const result = dropElements([1, 2, 3, 4], function(n) {return n >= 3;}) // should return [3, 4].
// const result = dropElements([0, 1, 0, 1], function(n) {return n === 1;}) // should return [1, 0, 1].
const result = dropElements([1, 2, 3], function(n) {return n > 0;}) // should return [1, 2, 3].
// const result = dropElements([1, 2, 3, 4], function(n) {return n > 5;}) // should return [].
// const result = dropElements([1, 2, 3, 7, 4], function(n) {return n > 3;}) // should return [7, 4].
// const result = dropElements([1, 2, 3, 9, 2], function(n) {return n > 2;}) // should return [3, 9, 2].

console.log(result); // [1, 2, 3]

