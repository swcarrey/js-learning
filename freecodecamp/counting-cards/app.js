/**
 * In the casino game Blackjack, a player can gain an advantage over the house by keeping track of the relative number of high and low cards remaining in the deck. This is called Card Counting.

Having more high cards remaining in the deck favors the player. Each card is assigned a value according to the table below. When the count is positive, the player should bet high. When the count is zero or negative, the player should bet low.

Count Change	Cards
+1            2, 3, 4, 5, 6
0	            7, 8, 9
-1	          10, 'J', 'Q', 'K', 'A'

You will write a card counting function. It will receive a card parameter, which can be a number or a string, and increment or decrement the global count variable according to the card's value (see table). The function will then return a string with the current count and the string Bet if the count is positive, or Hold if the count is zero or negative. The current count and the player's decision (Bet or Hold) should be separated by a single space.

Example Output
-3 Hold
5 Bet

Hint
Do NOT reset count to 0 when value is 7, 8, or 9.
Do NOT return an array.
Do NOT include quotes (single or double) in the output.
 */

var count = 0;

function cc(card) {
  // Only change code below this line
  const low = [2, 3, 4, 5, 6];
  const high = [10, 'J', 'Q', 'K', 'A'];

  // If card value is in low array
  if (low.indexOf(card) > -1) {
    // Increment count by 1
    count++;
  }
  
  // If card value is in high array
  if (high.indexOf(card) > -1) {
    // Decrement count by 1
    count--;
  }

  // Bet or hold depending on count
  let action = (count > 0) ? ' Bet' : ' Hold';
  // Wrap it all up
  return count + action;
  // Only change code above this line
}

// Add/remove calls to test your function.
// Note: Only the last will display

cc(3);
cc(7);
cc('Q');
cc(8);
console.log(cc('A'));