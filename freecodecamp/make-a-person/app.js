/*
Fill in the object constructor with the following methods below:

getFirstName() getLastName() getFullName() setFirstName(first) setLastName(last) setFullName(firstAndLast)
Run the tests to see the expected output for each method.

The methods that take an argument must accept only one argument and it has to be a string.

These methods must be the only available means of interacting with the object.
*/
var Person = function(firstAndLast) {

    let first = firstAndLast.split(' ')[0];
    let last = firstAndLast.split(' ')[1];
    
    this.setFullName = function(firstAndLast) {
        first = firstAndLast.split(' ')[0];
        last = firstAndLast.split(' ')[1];
    };

    // Initial setup of name
    this.setFullName(firstAndLast);

    this.setFirstName = function(name) {
        first = name;
    }

    this.setLastName = function(name) {
        last = name;
    }  

    this.getFullName = function() {
      return this.getFirstName() + ' ' + this.getLastName();
    };

    this.getFirstName = function() {
        return first;
    }

    this.getLastName = function() {
        return last;
    }
};
  
var bob = new Person('Bob Ross');

/*
[
    'setFullName', 'setFirstName', 'setLastName',
    'getFullName', 'getFirstName', 'getLastName'
]
*/
console.log(Object.keys(bob));

console.log(Object.keys(bob).length); // should return 6.
console.log(bob instanceof Person); // should return true.
console.log(bob.firstName); // should return undefined.
console.log(bob.lastName); // should return undefined.
console.log(bob.getFirstName()); // should return "Bob".
console.log(bob.getLastName()); // should return "Ross".
console.log(bob.getFullName()); // should return "Bob Ross".
console.log(bob.getFullName()); // should return "Haskell Ross" after bob.setFirstName("Haskell").
console.log(bob.getFullName()); // should return "Haskell Curry" after bob.setLastName("Curry").
console.log(bob.getFullName()); // should return "Haskell Curry" after bob.setFullName("Haskell Curry").
console.log(bob.getFirstName()); // should return "Haskell" after bob.setFullName("Haskell Curry").
console.log(bob.getLastName()); // should return "Curry" after bob.setFullName("Haskell Curry").