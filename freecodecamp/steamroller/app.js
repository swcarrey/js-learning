/*
https://learn.freecodecamp.org/javascript-algorithms-and-data-structures/intermediate-algorithm-scripting/steamroller

Intermediate Algorithm Scripting: Steamroller
Flatten a nested array. You must account for varying levels of nesting.
*/

/**
 * Flattens a nested array into a single array
 * 
 * @param {array} arr The array to flatten 
 * @returns {array}
 */
function steamrollArray(arr) {
    let result = [];

    (function iterate(arr) {
        arr.forEach(el => {
            // is an array, iterate again
            if (Array.isArray(el)) {
                iterate(el);
            }
            // push the value
            else {
                result.push(el);
            }
        });
    })(arr);

    return result;
}
  
const result = steamrollArray([[["a"]], [["b"]]]) // should return ["a", "b"].
// const result = steamrollArray([1, [2], [3, [[4]]]]) // should return [1, 2, 3, 4].
// const result = steamrollArray([1, [], [3, [[4]]]]) // should return [1, 3, 4].
// const result = steamrollArray([1, {}, [3, [[4]]]]) // should return [1, {}, 3, 4]

console.log(result); ['a', 'b'];

