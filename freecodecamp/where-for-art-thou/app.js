function whatIsInAName(collection, source) {
    let keys = Object.keys(source);

    return collection.filter(el => {

        // Could use for...in here instead and ditch Object.keys above
        for(let i = 0; i < keys.length; i++) {

            // Matching key values don't match - undefined if prop doesn't exist
            if (source[keys[i]] !== el[keys[i]]) {
                return false;
            }

        }
        return true;

    });
}

// const result = whatIsInAName([{ first: "Romeo", last: "Montague" }, { first: "Mercutio", last: null }, { first: "Tybalt", last: "Capulet" }], { last: "Capulet" });

// should return [{ "apple": 1, "bat": 2, "cookie": 2 }].
const result = whatIsInAName([{ "bat": 2,"apple": 1}, { "apple": 1 }, { "apple": 1, "bat": 2, "cookie": 2 }], { "apple": 1, "cookie": 2 });

console.log(result);