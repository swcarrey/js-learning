/*
Sum all the prime numbers up to and including the provided number.

A prime number is defined as a number greater than one and having only two divisors, one and itself. For example, 2 is a prime number because it's only divisible by one and two.

The provided number may not be a prime.
*/
function sumPrimes(num) {

    function isPrime(num) {

        if (num === 1) {
            return false;
        }
    
        for (let i = 2; i < num; i++) {
            if (num % i === 0) {
                return false;
            }
        }
        return true;
    }

    let arr = [];

    for (let x = 2; x <= num; x++) {
        if (isPrime(x)) {
            arr.push(x);
        }
    }

    return arr.reduce((acc, cur) => (acc + cur), 0);
}
  
const result = sumPrimes(20);

result


// FCC advanced - NOT MY CODE
function sumPrimes(num) {
    // step 1	
    let arr = Array.from({length: num + 1}, (v, k) => k).slice(2); 

    // step 2
    let onlyPrimes = arr.filter(n => { 
      let m = n - 1;
      
      while (m > 1 && m >= Math.sqrt(n)) { 
        if ((n % m) === 0) 
            return false;
            m--;
        }
        return true;
    });
    // step 3
    return onlyPrimes.reduce((acc, cur) => acc + cur); 
}

// test here
sumPrimes(3);

