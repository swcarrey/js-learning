/*
Return an English translated sentence of the passed binary string.

The binary string will be space separated.
*/

/**
 * 
 * One liner ES6
 * 
 * Split str at spaces into array
 * Parse each item from string into number as binary
 * Spread into fromCharCode
 * Return it...
 * 
 */
function binaryAgent(str) {
    return String.fromCharCode(...str.split(' ').map(el => parseInt(el, 2)));
}

// OR
// function binaryAgent(str) {
//     let arr = str.split(' ').map(function(el) {
//         return parseInt(el, 2);
//     });
//     return arr.map(el => String.fromCharCode(el)).join('');
// }
  
const result = binaryAgent("01000001 01110010 01100101 01101110 00100111 01110100 00100000 01100010 01101111 01101110 01100110 01101001 01110010 01100101 01110011 00100000 01100110 01110101 01101110 00100001 00111111") // should return "Aren't bonfires fun!?"
//const result = binaryAgent("01001001 00100000 01101100 01101111 01110110 01100101 00100000 01000110 01110010 01100101 01100101 01000011 01101111 01100100 01100101 01000011 01100001 01101101 01110000 00100001") // should return "I love FreeCodeCamp!"

console.log(result); // Arenn't bonfires fun!?