function convertHTML(str) {

    const lookup = {
        '&' : '&amp;',
        '<' : '&lt;',
        '>' : '&gt;',
        '\"' : '&​quot;', // no need to escape, just for clarity
        '\'' : '&​apos;'
    };

    // loop through special chars, do find and replace on string where they occur
    str.match(/[^A-Za-z0-9\s]/g) || [].forEach(el => {
        str = str.replace(el, lookup[el]);
    });

    return str;
}
  
// const result = convertHTML("Dolce & Gabbana") // should return Dolce &​amp; Gabbana.
// const result = convertHTML("Hamburgers < Pizza < Tacos") // should return Hamburgers &​lt; Pizza &​lt; Tacos.
// const result = convertHTML("Sixty > twelve") // should return Sixty &​gt; twelve.
const result = convertHTML('Stuff in "quotation marks"') // should return Stuff in &​quot;quotation marks&​quot;.
// const result = convertHTML("Schindler's List") // should return Schindler&​apos;s List.
// const result = convertHTML("<>") // should return &​lt;&​gt;.
// const result = convertHTML("abc") // should return abc

console.log(result); //  Stuff in &quot;quotation marks&quot;


/**
 * FCC advanced solution
 */
function convertHTML(str) {
    // Use Object Lookup to declare as many HTML entities as needed.
    htmlEntities={
      '&':'&amp;',
      '<':'&lt;',
      '>':'&gt;',
      '"':'&quot;',
      '\'':"&apos;"
    };
    //Use map function to return a filtered str with all entities changed automatically.
    return str.split('').map(entity => htmlEntities[entity] || entity).join('');
}

// Which one do I like best...?
