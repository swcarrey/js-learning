function titleCase(str) {
    return str.toLowerCase()
              .split(' ')
              // Get first letter, uppercase it, concantenate with 2nd letter onwards
              .map(el => el.slice(0, 1).toUpperCase() + el.slice(1))
              .join(' ');
}
  
const result = titleCase("I'm a little tea pot");

result

