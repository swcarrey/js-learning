function destroyer(arr) {
    const toRemove = [...arguments].slice(1);
    // If el is in the args array, dont return it
    return arr.filter(el => !(toRemove.indexOf(el) >= 0));
}
  
const result = destroyer([1, 2, 3, 1, 2, 3], 2, 3);
console.log(result);

/************************
 * OR, using rest param
 ************************/

function destroyer2(arr, ...args) {
    // If el is in the args array, dont return it
    return arr.filter(el => !(args.indexOf(el) >= 0));
}
  
const result2 = destroyer([1, 2, 3, 1, 2, 3], 2, 3);
console.log(result2);
