function convertToRoman(num) {

    const numeralMap = {
        1: 'I',
        4: 'IV',
        5: 'V',
        9: 'IX',
        10: 'X',
        40: 'XL',
        50: 'L',
        90: 'XC',
        100: 'C',
        400: 'CD',
        500: 'D',
        900: 'CM',
        1000: 'M'
    };

    /**
     * Finds highest key in numeralMap that goes into x
     * 
     * @param {int} x Number to test
     * @returns {int} The largest number
     */
    function getHighestIn(x) {
        let tmp = 0;
        for (let prop in numeralMap) {
            if (prop <= x) {
                tmp = prop;
            }
        }
        return tmp;
    };

    let numeralString = '';

    while (num > 0) {
        let highest = getHighestIn(num);
        numeralString += numeralMap[highest];
        num -= highest;
    }

    return numeralString;
}


// const result = convertToRoman(2); // should return "II".
// const result = convertToRoman(3); // should return "III".
// const result = convertToRoman(4); // should return "IV".
// const result = convertToRoman(5); // should return "V".
// const result = convertToRoman(9); // should return "IX".
// const result = convertToRoman(12); // should return "XII".
// const result = convertToRoman(16); // should return "XVI".
// const result = convertToRoman(29); // should return "XXIX".
// const result = convertToRoman(44); // should return "XLIV".
// const result = convertToRoman(45); // should return "XLV"
// const result = convertToRoman(68); // should return "LXVIII"
// const result = convertToRoman(83); // should return "LXXXIII"
// const result = convertToRoman(97); // should return "XCVII"
// const result = convertToRoman(99); // should return "XCIX"
// const result = convertToRoman(400); // should return "CD"
// const result = convertToRoman(500); // should return "D"
// const result = convertToRoman(501); // should return "DI"
// const result = convertToRoman(649); // should return "DCXLIX"
// const result = convertToRoman(798); // should return "DCCXCVIII"
// const result = convertToRoman(891); // should return "DCCCXCI"
// const result = convertToRoman(1000); // should return "M"
// const result = convertToRoman(1004); // should return "MIV"
// const result = convertToRoman(1006); // should return "MVI"
// const result = convertToRoman(1023); // should return "MXXIII"
// const result = convertToRoman(2014); // should return "MMXIV"
const result = convertToRoman(3999); // should return "MMMCMXCIX"

console.log(result); // MMMCMXCIX

