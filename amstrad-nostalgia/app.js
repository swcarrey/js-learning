const phrase = "It started with Acorn computers back in school. Remember 20 GOTO 10?|Then my parents bought me a Spectrum +2 complete with a joystick and games on tape! What a day!|Next up was an ageing Amstrad which I used to type out my dads' estimates on|Then Christmas came early... enter Commodore Amiga 500, I loved that thing!|But eventually a new toy came along...|A PC - loaded up with win95, fantastic colours, amazing graphics, videos, games, could it get better?|Then came win98 > winXP > vista > 7 > 8 > 8.1 > 10...|A Mac Mini, a Macbook, a raspberry pi...|Tried various Linux distros, bluetooth was ropey at best, nor could I get my e1000e ethernet to connect out the box without some pre-installation sorcery. Music software was an absolute no no :(|So what now? How do I rekindle the sort of enjoyment I got when turning on that Amiga back in the day?| By learning programming of course!|THANKS FOR WATCHING :)";

const el = document.getElementById('el');
const phraseArr = phrase.split('');

/**
 * Currrent character counter
 */
let index = 0;

/**
 * Only run one setTimeout at a time
 * 
 * NEEDS FIXING
 */
setTimeout(function typeLetter(){
  // Stop cursor blink
  el.classList.remove('blink');

  el.innerHTML += phraseArr[index];
  index++;

  if (index < phraseArr.length) {
    // Humanize typing
    let time = randomRange(80, 150);
    
    if (phraseArr[index] === '|') {
      // Add break
      el.innerHTML += '</br>';
      // Blink cursor
      el.classList.add('blink');
      // Skip printing character
      index++;
      // Set longer delay
      time = 3000;
    }

    // Previous character is a dot but not more than one in a row
    if (phraseArr[index - 1] === '.' && phraseArr[index] !== '.') {
      time = 800;
    }
    
    const delayedChars = ['!', '?', '>'];
    // Character is in array, delay the print
    if (delayedChars.indexOf(phraseArr[index]) !== -1) {
      time = 300;
    }

    /**
     * Check again in case index++ above exceeds array length
     */
    if (index < phraseArr.length) {
      setTimeout(typeLetter, time);
    }
  }
  // End of phrase, add blink
  else {
    el.classList.add('blink');
  }
}, 0)

function randomRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}