window.onload = function() {
  animate();
 };

const animate = function(element) {
  const textToSwitch = document.getElementById('text-to-switch');
  let counter = 0;

  // The words
  const textOptions = ['cats', 'dogs', 'bears', 'fish', 'foxes'];

  // Duration
  const ms = 3000;

  // timeout
  const timeout = ms - 1000;

  // Switch the text every X seconds
  function repeatingProcess() {
    setInterval(function() {
      textToSwitch.classList.remove('out');
      textToSwitch.classList.add('in');
      switchText();   
    }, ms);
  }

  /**
   * Switch text and remove effect class after
   * animation has finished
   */
  function switchText() {
    counter = counter % textOptions.length;
    setTimeout(function() {
      textToSwitch.classList.remove('in');
    }, timeout);
    setTimeout(function() {
      textToSwitch.classList.add('out');
    }, timeout);
    textToSwitch.textContent = textOptions[counter];
    counter++;
  }
  
  // Run the repeating process function
  repeatingProcess();
}

